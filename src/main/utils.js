
/* eslint-disable */
import path from 'path'
import fs from 'fs-extra'
const { ipcMain } = require('electron')

export function findShot (dir, step = 'ani') {
  const scs = fs.readdirSync(dir)
  let shotDirs = []
  for (const sc of scs) {
    const scDir = path.join(dir, sc)
    const isDir = fs.lstatSync(scDir).isDirectory()
    if (isDir) {
      const shots = fs.readdirSync(scDir)
      const cShots = shots.map(shot => {
        const shotDir = path.join(scDir, shot, step)
        return {dir: shotDir, name: shot}
      })
      shotDirs = shotDirs.concat(cShots)
    }
  }
  return shotDirs
}
// (\w+\.ani.animation).v([0-9]{3})
export function findTarget (shotSteps, regexp) {
  const result = {}
  for (const shot of shotSteps) {
    const sDir = shot.dir
    if (fs.existsSync(sDir)) {
      const dirs = fs.readdirSync(sDir)
      for (const item of dirs) {
        const matches = item.match(regexp)
        if (matches) {
          const fullname = matches[1]
          const version = matches[2]
          if (!result[fullname]) {
            result[fullname] = {shot: shot.name, fullname, maxV: '000', versions: [], dir: sDir}
          }
          result[fullname].versions.push(version)
          if (Number(version) > Number(result[fullname].maxV)) {
            result[fullname].maxV = version
          }
        }
      }
    }
  }
  return Object.keys(result).map(key => result[key])
}

function start (config) {
  const shots = findShot(config.root, config.step)
  const target = findTarget(shots, config.regexp)
  return target
}
export default function listen () {
  ipcMain.on('search', async (event, config) => {
    const result = start(config)
    return result
  })
}
