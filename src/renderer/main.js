import Vue from 'vue'

import App from './App'

// element
import {
  Notification, Button
} from 'element-ui'

Vue.prototype.$notify = Notification
;([
  Button
]).map((Comp) => {
  Vue.use(Comp)
})
Vue.prototype.$ELEMENT = { size: 'mini', zIndex: 3000 }
// element end

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  components: { App },
  template: '<App/>'
}).$mount('#app')
